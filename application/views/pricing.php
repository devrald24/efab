<section class="home-contact bg-white py-5">
    <div class="container">
        <div class="text-center mb-3">
            <h2 class="font-weight-bold text-center">Pricing</h2>
        </div>
<div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row pricing-plan">
                            <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                <div class="pricing-box">
                                    <div class="pricing-body b-l">
                                        <div class="pricing-header">
                                            <h4 class="text-center">BASIC</h4>
                                            <h2 class="text-center"><span class="price-sign">$</span>14.95</h2>
                                            <p class="uppercase">per month</p>
                                        </div>
                                        <div class="price-table-content">
                                            <div class="price-row">TRY IT OUT</div>
                                            <div class="price-row px-3">View all Attachments + up to 5 Bids</div>
                                            <div class="price-row">
                                                <button class="btn btn-success waves-effect waves-light m-t-20">Sign up</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                <div class="pricing-box b-l">
                                    <div class="pricing-body">
                                        <div class="pricing-header">
                                            <h4 class="text-center">STANDARD</h4>
                                            <h2 class="text-center"><span class="price-sign">$</span>28.95</h2>
                                            <p class="uppercase">per month</p>
                                        </div>
                                        <div class="price-table-content">
                                            <div class="price-row">POPULAR</div>
                                            <div class="price-row px-3">View all Attachments + up to 30 Bids</div>
                                            <div class="price-row">
                                                <button class="btn btn-success waves-effect waves-light m-t-20">Sign up</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                <div class="pricing-box featured-plan">
                                    <div class="pricing-body">
                                        <div class="pricing-header">
                                            <h4 class="price-lable text-white bg-warning">Popular</h4>
                                            <h4 class="text-center">PREMIUM</h4>
                                            <h2 class="text-center"><span class="price-sign">$</span>44.50</h2>
                                            <p class="uppercase">per month</p>
                                        </div>
                                        <div class="price-table-content">
                                            <div class="price-row">FREQUENT USE</div>
                                            <div class="price-row px-3"> View all Attachments + up to 70 Bids</div>
                                            <div class="price-row">
                                                <button class="btn btn-lg btn-info waves-effect waves-light m-t-20">Sign up</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                <div class="pricing-box">
                                    <div class="pricing-body b-r">
                                        <div class="pricing-header">
                                            <h4 class="text-center">BULK</h4>
                                            <h2 class="text-center"><span class="price-sign">$</span>365</h2>
                                            <p class="uppercase">per year</p>
                                        </div>
                                        <div class="price-table-content">
                                            <div class="price-row">ECONOMICAL</div>
                                            <div class="price-row px-2">View all Attachments + up to 1000 Bids</div>
                                            <div class="price-row">
                                                <button class="btn btn-success waves-effect waves-light m-t-20">Sign up</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>