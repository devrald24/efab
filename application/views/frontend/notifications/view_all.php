<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title font-weight-bold mb-0">Notifications</h3>
            </div>
            <ul class="list-group list-group-flush">
            <li class="list-group-item py-3 px-3">
                <div class="d-flex flex-row justify-content-between">
                    <div>
                        You were invited to the job <a href="<?php echo base_url('jobs/1') ?>">[URGENT] Need 10 Engineers</a>
                    </div>
                    <div>
                        <small class="text-muted">2 hours</small>
                    </div>
                </div>
            </li>
            <li class="list-group-item py-3 px-3">
                <div class="d-flex flex-row justify-content-between">
                    <div>
                        You were invited to the job <a href="<?php echo base_url('jobs/1') ?>">[URGENT] Need 10 Engineers</a>
                    </div>
                    <div>
                        <small class="text-muted">2 hours</small>
                    </div>
                </div>
            </li>
            <li class="list-group-item py-3 px-3">
                <div class="d-flex flex-row justify-content-between">
                    <div>
                        You were invited to the job <a href="<?php echo base_url('jobs/1') ?>">[URGENT] Need 10 Engineers</a>
                    </div>
                    <div>
                        <small class="text-muted">2 hours</small>
                    </div>
                </div>
            </li>
            </ul>
        </div>
        </div>
    </div>
</div>